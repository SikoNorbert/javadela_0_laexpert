package ExercitiiPropuseSpreImplementare;
import java.util.Scanner;
 class NumereRationale {
    double numitor, numarator;

    NumereRationale(){
        super();
    }
    NumereRationale(double numitor){
        this.numitor = numitor;
    }
    NumereRationale(double numitor, double numarator){
        this.numitor = numitor;
        this.numarator = numarator;
    }

    public String adunare(double numitor, double numarator){
        return "The number adds: " + (numitor + numarator);
    }
    public String scadere(double numitor, double numarator){
        return "The number differences: " + (numitor - numarator);
    }
    public String impartire(double numitor, double numarator){
        return "The number divided : " + (numarator / numitor);
    }
    public String inmultire(double numitor, double numarator){
        return "The number multiplies : " + (numarator * numitor);
    }
}

class Auxiliar {
    double numitor;
    double numarator;
    Scanner scanner = new Scanner(System.in);
    int n = scanner.nextInt();

    public void cmmmc(double numarator) {
            if ((numarator % n) == 0) {
                int a = n + 2;
                System.out.println("numarator = " + numarator + " = " + (Math.pow(2, n + a)));
        }
    }
    public void cmmdc(double numarator){
            if ((numarator % n) == 0){
                System.out.println("numarator = " + numarator + " = " + (Math.pow(n, 0)));
        }
    }
}
public class Implementation {
    public static void main(String[] args) {
        NumereRationale a = new NumereRationale();
        NumereRationale b = new NumereRationale(16);
        NumereRationale c = new NumereRationale(16, 32);
        System.out.println(c.adunare(16, 32));
        System.out.println(c.scadere(100, 50));
        System.out.println(c.impartire(50, 100));
        System.out.println(c.inmultire(100, 1000));

        Auxiliar auxiliar = new Auxiliar();
        auxiliar.cmmmc(32);
        auxiliar.cmmdc(32);
    }
}
