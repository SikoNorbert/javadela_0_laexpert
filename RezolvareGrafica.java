package ExercitiiPropuseSpreImplementare;

 class Punct {
    protected int x, y;

    public Punct(int x, int y) {
        int punctOrizontal = x;
        int punctVertical = y;
    }

    public int muta(int dx, int dy) {
        int mutaOrizontalPlus = x + dx;
        int mutaVerticalPlus = y + dy;
        int mutaOrizontalMinus = x - dx;
        int mutaVerticalMinus = y - dy;
        return dx + dy;
    }

    public String toString() {
        if (x == 0 || y == 0) {
            x = 20;
            y = 20;
        }
        return x + "+" + y;
    }
}

class Dreptunghi{
    protected int x, y, x0, y0;

    public Dreptunghi(int x, int y, int x0, int y0) {
        int dreptUnghiX = x;
        int dreptUnghiY = y;
        int dreptUnghiX0 = x0;
        int dreptUnghiY0 = y0;
    }

    public int muta(int dx, int dy) {
        int dreptUnghiXPlus = x + dx;
        int dreptUnghiXMinus = x - dx;
        int dreptUnghiYPlus = y + dy;
        int dreptUnghiYMinus = y - dy;
        int dreptUnghiX0Plus = x0 + dx;
        int dreptUnghiX0Minus = x0 - dx;
        int dreptUnghiY0Plus = y0 + dy;
        int dreptUnghiY0Minus = y0 - dy;
        return dx + dy;
    }

    public String toString() {
        if (x == 0 || y == 0 || x0 == 0 || y0 == 0) {
            x = 15;
            y = 15;
            x0 = 15;
            y0 = 15;
        }
        return x + "+" + y + "+" + x0 + "+" + y0;
    }
}

class Cerc{
    protected int x, y, r;

    public Cerc(int x, int y, int r) {
        int CercX = x;
        int CercY = y;
        int CercRaza = r;
    }

    public int muta(int dx, int dy, int teta) {
        int mutaXPlus = x + dx;
        int mutaXMinus = x - dx;
        int mutaYPlus = y + dy;
        int mutaYMinus = y - dy;
        int a = (int) (Math.pow(x, x) + Math.pow(y, y));
        r = (int) (Math.sqrt(a) * Math.cos(teta));
        return dx + dy + teta;
    }

    public String toString() {
        if(x == 0 || y == 0 || r == 0){
            x = 25;
            y = 25;
            r = 30;
        }
        return x + "+" + y + "+" + r;
    }
}

public class RezolvareGrafica {
    public static void main(String[] args) {
        Punct punct = new Punct(20,20);
        System.out.println("dx + dy = " + punct.muta(10,10));
        System.out.println(punct.toString());

        Dreptunghi dreptunghi = new Dreptunghi(15,15,15,15);
        System.out.println("dx + dy = " + dreptunghi.muta(15,15));
        System.out.println(dreptunghi.toString());

        Cerc cerc = new Cerc(25,25,30);
        System.out.println("dx + dy + teta = " +cerc.muta(15,15,30));
        System.out.println(cerc.toString());
    }

}


