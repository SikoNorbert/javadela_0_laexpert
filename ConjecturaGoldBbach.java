package ConjecturaLuiGoldbach;

import java.util.Scanner;

public class ConjecturaGoldBbach {

    public static void main(String[] args) {
        Scanner number = new Scanner(System.in);
        System.out.println("Enter the first number:(startpoint) ");
        int m = number.nextInt();
        System.out.println("Enter the second number:(endpoint) ");
        int n = number.nextInt();
        System.out.println("List of prime numbers between " + m + " and " + n);
        for (int i = m; i <= n; i++) {
            if (conjectura(i)) {
                System.out.println(i);
            }
        }
        for (int j = 3; j <= n; j = j + 2) {
            for (int i = m; i <= n; i++) {
                int diffNumber = i - n;

                System.out.println("The prime sum numbers is " + " sum = " + j + "+" + diffNumber);
            }
        }
    }
    public static boolean conjectura(int k) {
        if (k <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(k); i++) {
            if (k % i == 0) {
                return false;
            }
        }
        return true;
    }
}