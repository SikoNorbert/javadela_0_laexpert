package Joc_hartie_pumn_foarfece;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
    static String[] string = {"rock", "paper", "scissor"};
    static List<String> Person = new ArrayList<String>();
    static List<String> Calculator = new ArrayList<String>();
    static List<String> Calculator2 = new ArrayList<String>();
    static int scorPerson = 0, scorCalculator = 0, scorCalculator2 = 0;

    public static void main(String[] args) {
        int PersonalID = 1;
        int CaculatorID = 2;
        int Calculator2ID = 3;
        Calculator.add(randomizedMethod());
        Calculator2.add(randomizedMethod());
        Person.add("rock");
        Person.add("paper");
        Person.add("scissor");

        Calculator.add("rock");
        Calculator.add("paper");
        Calculator.add("scissor");
        Scanner scanner = new Scanner(System.in);
        System.out.println("De cate ori jucam (?) : ");
        int eventList = scanner.nextInt();

        matchesPerson_Calculator(eventList);
        matchesCalculator_Calculator(eventList);
    }

    public static String randomizedMethod() {
        Random random = new Random();
        String randomString = string[random.nextInt(string.length)];
        return randomString;
    }

    public static void matchesPerson_Calculator(int eventList) {
        try {
            for (int i = 0; i < eventList; i++) {
                if (Person.get(i) == Calculator.get(i)) {
                    scorPerson = 0;
                    System.out.println("Daca personID = CalculatorID : " + scorPerson);
                }
                comparePerson_Calculator(eventList);
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Mesajul meu este: " + e);
        }
    }

    public static void matchesCalculator_Calculator(int eventList) {
        try {
            for (int i = 0; i < eventList; i++) {
                if (Calculator.get(i) == Calculator2.get(0)) {
                    scorCalculator = 0;
                    System.out.println("Daca CalculatorID = Calculator2ID: " + scorCalculator);
                }
                compareCalculator_Calculator(eventList);
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Mesajul meu este asta: " + e);
        }
    }

    public static void comparePerson_Calculator(int eventList) {
        for (int i = 0; i < eventList; i++) {
            if (Person.get(0).equals("rock") == Calculator.get(i).equals("paper")) {
                scorCalculator++;
            }
            if (Person.get(1).equals("paper") == Calculator.get(i).equals("scissor")) {
                scorCalculator++;
            }
            if (Person.get(2).equals("scissor") == Calculator.get(i).equals("rock")) {
                scorCalculator++;
            } else {
                if (Person.get(0).equals("rock") == Calculator.get(i).equals("scissor")) {
                    scorPerson++;
                }
                if (Person.get(1).equals("paper") == Calculator.get(i).equals("rock")) {
                    scorPerson++;
                }
                if (Person.get(2).equals("scissor") == Calculator.get(i).equals("paper")) {
                    scorPerson++;
                }
                System.out.println("Scor personal: " + scorPerson);
            }
            System.out.println("Scor calculator: " + scorCalculator);
        }
    }

    public static void compareCalculator_Calculator(int eventList) {
        for (int i = 0; i < eventList; i++) {
            if (Calculator.get(0).equals("rock") == Calculator2.get(0).equals("paper")) {
                scorCalculator2++;
            }
            if (Calculator.get(1).equals("paper") == Calculator2.get(0).equals("scissor")) {
                scorCalculator2++;
            }
            if (Calculator.get(2).equals("scissor") == Calculator2.get(0).equals("rock")) {
                scorCalculator2++;
            } else {
                if (Calculator.get(0).equals("rock") == Calculator2.get(0).equals("scissor")) {
                    scorCalculator++;
                }
                if (Calculator.get(1).equals("paper") == Calculator2.get(0).equals("rock")) {
                    scorCalculator++;
                }
                if (Calculator.get(2).equals("scissor") == Calculator2.get(0).equals("paper")) {
                    scorCalculator++;
                }
                System.out.println("Scor calculator_calculator2 al scorului calculator: " + scorCalculator);
            }
            System.out.println("Scor calculator 2: " + scorCalculator2);
        }
    }
}