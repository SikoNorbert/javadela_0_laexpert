package ExercitiiPropuseSpreImplementare;

public class Complex {

    private double re, im;

    public Complex() {
        this.re = re;
        this.im = im;
    }

    public Complex(double re) {
        this.re = re;
    }

    public Complex(double re, double im) {
        this.re = re;
        this.im = im;
    }

    public Complex suma(Complex z) {
        double real = this.re + z.re;
        double imag = this.im + z.im;
        return new Complex(real, imag);
    }

    public Complex produs(Complex z) {
        double real = this.re * z.re;
        double imag = this.im * z.im;
        return new Complex(real, imag);
    }

    public double modul() {
        double z = Math.pow(this.re, this.re) + Math.pow(this.im, this.im);
        double modulus = Math.sqrt(z);
        return modulus;
    }

    public String toString() {
        if (im == 0) return re + "";
        if (re == 0) return im + "i";
        if (im < 0) return re + " - " + (-im) + "i";
        return re + " + " + im + "i";
    }

    public static void main(String[] args) {
        Complex complex = new Complex();
        Complex complexRe = new Complex(10);
        Complex a = new Complex(2, 3);
        Complex b = new Complex(-3, 2);
        System.out.println("Suma a doua numere complexe: " + a.suma(b));
        System.out.println("Produsul a doua numere complexe: " + a.produs(b));
        System.out.println("Modulul primului numere complexe: " + a.modul());
        System.out.println("Modulul al doilea numere complexe: " + b.modul());
        System.out.println("a= " + a.toString());
        System.out.println("b= " + b.toString());
    }
}