package ExercitiiPropuseSpreImplementare;

import java.util.Scanner;

public class ContBancar {
    public String contBancar;
    public String tipCont;
    public String numeleClientului;
    public String adresaClientului;
    public double sumaDeBani;
    public double dobanda;

    public ContBancar(String contBancar, String tipCont, String numeleClientului, String adresaClientului) {
        this.contBancar = contBancar;
        this.tipCont = tipCont;
        this.numeleClientului = numeleClientului;
        this.adresaClientului = adresaClientului;
    }



    public double depunere(double sumaDeBani) {
        System.out.println("Suma de bani initial la depunere: ");
        return sumaDeBani;
    }

    public double retragere(double retragere) {
        System.out.println("Suma de bani la retragere: ");
        return retragere;
    }

    public void capital(double sumaDeBani, double retragere) {
        System.out.println("Aflarea sumei de bani curente: ");
        System.out.print(sumaDeBani - retragere);
    }

    public double dobanda(double dobanda) {
        System.out.println("\nDobanda este: ");
        return dobanda;
    }
}

class TipuriDeConturi extends ContBancar {
    static int catePersoane = 0;
    String contBancar;
    Scanner scanner = new Scanner(System.in);

    public TipuriDeConturi(String contBancar, String tipCont, String numeleClientului, String adresaClientului) {
        super(contBancar, tipCont, numeleClientului, adresaClientului);
    }

    public void operatiuniPeBancaZilnice(String numeleClientului) {
        if (depunere(-2000) < 0) {
            double scadereaSumeiDeBani = depunere(-2000) - dobanda(1000);
            System.out.println("Scaderea sumei de bani : " + scadereaSumeiDeBani);
        }
    }

    public void retinereaSumeiDeBaniPeTermenLung() {
        for (int i = 1; i <= 12; i++) {
            double sumaTotala = depunere(5555) + (depunere(5555) * 0.05);
            System.out.println("Suma totala de bani dupa 1 an: " + sumaTotala);
        }
    }

    public int numarareaPersoanelorFiziceCareAuContBancarDeschis(String numeleClientului) {
        System.out.println("Persoanele fizice care au cont bancar: ");
        contBancar = scanner.nextLine();
        if (numeleClientului.contains(contBancar)) {
        }
        return catePersoane++;
    }
}

class ApelareaContuluiBancar {
    public static void main(String[] args) {
        ContBancar contbancar = new ContBancar("OTP16753252774", "Ordin de Plata", "Siko Norbert", "Reghin,str.Baii,nr.22");
        System.out.println(contbancar.depunere(5555));
        System.out.println(contbancar.retragere(2555));
        contbancar.capital(5555, 2555);
        System.out.println(contbancar.dobanda(1000));
        TipuriDeConturi tipuriDeConturi = new TipuriDeConturi("BT7436634734", "Ordin de Plata", "Alex Ion Fierascu", "Iasi");
        tipuriDeConturi.operatiuniPeBancaZilnice("Ion Fierascu");
        tipuriDeConturi.retinereaSumeiDeBaniPeTermenLung();
        System.out.println(tipuriDeConturi.numarareaPersoanelorFiziceCareAuContBancarDeschis("Siko Norbert"));
    }
}
